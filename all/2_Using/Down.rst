.. index:: Download

.. _download:

Download a resource
###################

You can download a resource provided in the ELG platform through the Download tab:

.. figure:: Download.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/resource/service/corpus/913
   :alt: Download UI

Downloading a resource is subject to agreeing with the licensing terms under which it is provided, while additional actions may also be required (for instance, access only for registered users):

.. figure:: LicenseTerms.png
   :width: 800px
   :align: center
   :alt: License terms UI