.. _TryOut:

Use an LT service
#########################

LT services that run in the cloud and follow ELG's specifications can be used via the ELG platform. You can do this via the try-out UI directly in the catalogue, or via ELG's API.

.. index:: Try Out

Try-out UI
----------

The catalogue entry of a service has a tab called **Test/Try out**:

**Test/Try out**:

.. figure:: TryOutUI.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/resource/service/tool/480
   :alt: Try out UI

Here you can provide a sample input and see the results output by the service. Depending on the type of the service, you can type in or paste some text, or upload or record audio, and get the results rendered in a task-specific viewer.

.. note:: In the current release, only registered users can try out services.
   
.. index:: Code sample

Call a service via the API
--------------------------

The catalogue entry of a service has a tab called **Code samples**:

.. figure:: TryOutUICodeSamples.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/resource/service/tool/480
   :alt: Try out UI

You can copy and modify the provided example commands to call the service from your command line.

More information on how to call and test services is given in the :ref:`PublicAPI`.