.. _contributing:

Contributing to ELG
###################

This chapter is for **providers**, i.e. for users who wish to contribute language resources and technologies as well as information about organizations and projects to ELG. You will learn how to register as a provider and how to contribute each type of entity.