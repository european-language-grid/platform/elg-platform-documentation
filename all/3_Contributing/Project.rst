.. _contributeProject:

Contribute a project
####################################

This page describes how to contribute information for a :ref:`project<typesOfResources>` to the European Language Grid.

Before you start
----------------

- Please make sure that the information you want to contribute complies with our :ref:`terms of use <termsOfUse>`.
- Please make sure you have :ref:`registered <register>` and been assigned the :ref:`provider role<providerRole>`.

Step 1: create metadata
-----------------------

The first step is to describe your project using ELG’s metadata format, ELG-SHARE. Future releases of ELG will include an interactive editor for this. However, for now, you must create an XML file. Refer to the examples below for how to do this.

The elements you need are documented on the following pages:

- :ref:`minimalAll`
- :ref:`minimalProject`

For more information about ELG-SHARE, see:

- :ref:`basicSchema`

At the ELG GitLab, you will find `templates <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20templates>`_ (that you can use to create new metadata records) and `examples <https://gitlab.com/european-language-grid/platform/ELG-SHARE-schema/-/tree/master/metadata%20record%20examples>`_ in XML format.



Example 1
^^^^^^^^^

Bergamot – Browser-based Multilingual Translation

Published at: https://live.european-language-grid.eu/catalogue/#/resource/projects/392

.. code-block:: xml

	<?xml version="1.0" encoding="UTF-8"?>
	<ms:MetadataRecord xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ ../../Schema/ELG-SHARE.xsd" xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned - leave as is</ms:MetadataRecordIdentifier>
		<ms:metadataCreationDate>2020-01-07</ms:metadataCreationDate>
		<ms:metadataLastDateUpdated>2020-01-07</ms:metadataLastDateUpdated>
		<!-- the metadataCurator is the person responsible for editing/updating the metadata record in the ELG system and maybe different from metadata creator (for metadata records harvested from other repos, there will be no metadata creator) -->
		<ms:metadataCurator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
			<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
			<ms:email>smith@example.com</ms:email>
		</ms:metadataCurator>
		<ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
		<ms:metadataCreator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
			<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
			<ms:email>smith@example.com</ms:email>
		</ms:metadataCreator>
		<ms:DescribedEntity>
			<ms:Project>
				<ms:entityType>Project</ms:entityType>
				<ms:ProjectIdentifier ms:ProjectIdentifierScheme="http://w3id.org/meta-share/meta-share/cordis">219608</ms:ProjectIdentifier>
				<ms:projectName xml:lang="en">Browser-based Multilingual Translation</ms:projectName>
				<ms:projectShortName xml:lang="en">Bergamot</ms:projectShortName>
				<ms:fundingType>http://w3id.org/meta-share/meta-share/euFunds</ms:fundingType>
				<ms:funder>
					<ms:Organization>
						<ms:actorType>Organization</ms:actorType>
						<ms:organizationName xml:lang="en">European Commission</ms:organizationName>
						<ms:website>https://ec.europa.eu/info/index_en</ms:website>
					</ms:Organization>
				</ms:funder>
				<ms:fundingCountry>EU</ms:fundingCountry>
				<ms:projectStartDate>2019-01-01</ms:projectStartDate>
				<ms:projectEndDate>2021-12-31</ms:projectEndDate>
				<ms:website>https://browser.mt/</ms:website>
				<ms:logo>https://ufal.mff.cuni.cz/sites/default/files/styles/drupal_projects_logo_style/public/bergamot_logo.png</ms:logo>
				<ms:LTArea>
					<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
				</ms:LTArea>
				<ms:LTArea>
					<ms:LTClassOther>Browser-based Machine Translation</ms:LTClassOther>
				</ms:LTArea>
				<ms:domain>
					<ms:categoryLabel xml:lang="en">htttp://w3id.org/meta-share/omtd-share/NewsMediaJournalismAndPublishing</ms:categoryLabel>
				</ms:domain>
				<ms:domain>
					<ms:categoryLabel xml:lang="en">General</ms:categoryLabel>
				</ms:domain>
				<ms:keyword xml:lang="en">Machine translation</ms:keyword>
				<ms:keyword xml:lang="en">translation integration</ms:keyword>
				<ms:grantNumber>825303</ms:grantNumber>
				<ms:projectSummary xml:lang="en">'The Bergamot project will add and improve client-side machine translation in a web browser.  Unlike current cloud-based options, running directly on users'' machines empowers citizens to preserve their privacy and increases the uptake of language technologies in Europe in various sectors that require confidentiality.  Free software integrated with an open-source web browser, such as Mozilla Firefox, will enable bottom-up adoption by non-experts, resulting in cost savings for private and public sector users who would otherwise procure translation or operate monolingually.  To understand and support non-expert users, our user experience work package researches their needs and creates the user interface.  Rather than simply translating text, this interface will expose improved quality estimates, addressing the rising public debate on algorithmic trust.  Building on quality estimation research, we will enable users to confidently generate text in a language they do not speak, enabling cross-lingual online form filling.  To improve quality overall, dynamic domain adaptation research addresses the peculiar writing style of a website or user by adapting translation on the fly using local information too private to upload to the cloud.  These applications require adaptation and inference to run on desktop hardware with compact model downloads, which we address with neural network efficiency research.  Our combined research on user experience, domain adaptation, quality estimation, outbound translation, and efficiency support a broad browser-based innovation plan.'</ms:projectSummary>
				<ms:cost>
					<ms:amount>2999096.25</ms:amount>
					<ms:currency>http://w3id.org/meta-share/meta-share/euro</ms:currency>
				</ms:cost>
				<ms:ecMaxContribution>
					<ms:amount>2999096.25</ms:amount>
					<ms:currency>http://w3id.org/meta-share/meta-share/euro</ms:currency>
				</ms:ecMaxContribution>
				<ms:fundingSchemeCategory>RIA</ms:fundingSchemeCategory>
				<ms:status>SIGNED</ms:status>
				<ms:relatedCall>H2020-ICT-2018-2</ms:relatedCall>
				<ms:relatedProgramme>H2020</ms:relatedProgramme>
				<ms:relatedSubprogramme>ICT-29-2018</ms:relatedSubprogramme>
				<ms:coordinator>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">THE UNIVERSITY OF EDINBURGH</ms:organizationName>
					<ms:website>https://www.ed.ac.uk/</ms:website>
				</ms:coordinator>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">TARTU ULIKOOL</ms:organizationName>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">MZ DENMARK APS</ms:organizationName>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">THE UNIVERSITY OF SHEFFIELD</ms:organizationName>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">UNIVERZITA KARLOVA</ms:organizationName>
					<ms:website>https://www.cuni.cz/</ms:website>
				</ms:participatingOrganization>
			</ms:Project>
		</ms:DescribedEntity>
	</ms:MetadataRecord>

Example 2
^^^^^^^^^

European Language Grid

Published at: https://live.european-language-grid.eu/catalogue/#/resource/projects/395

.. code-block:: xml

	<?xml version="1.0" encoding="UTF-8"?>
	<ms:MetadataRecord xsi:schemaLocation="http://w3id.org/meta-share/meta-share/ ../../Schema/ELG-SHARE.xsd" xmlns:ms="http://w3id.org/meta-share/meta-share/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">value automatically assigned - leave as is</ms:MetadataRecordIdentifier>
		<ms:metadataCreationDate>2020-01-07</ms:metadataCreationDate>
		<ms:metadataLastDateUpdated>2020-01-07</ms:metadataLastDateUpdated>
		<!-- the metadataCurator is the person responsible for editing/updating the metadata record in the ELG system and maybe different from metadata creator (for metadata records harvested from other repos, there will be no metadata creator) -->
		<ms:metadataCurator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
			<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
			<ms:email>smith@example.com</ms:email>
		</ms:metadataCurator>
		<ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
		<ms:metadataCreator>
			<ms:actorType>Person</ms:actorType>
			<ms:surname xml:lang="en">Smith</ms:surname>
			<ms:givenName xml:lang="en">John</ms:givenName>
			<!-- please add an identifier (preferrably ORCID in the format below) and/or email -->
			<ms:PersonalIdentifier ms:PersonalIdentifierScheme="http://purl.org/spar/datacite/orcid">0000-0000-0000-0000</ms:PersonalIdentifier>
			<ms:email>smith@example.com</ms:email>
		</ms:metadataCreator>
		<ms:DescribedEntity>
			<ms:Project>
				<ms:entityType>Project</ms:entityType>
				<ms:ProjectIdentifier ms:ProjectIdentifierScheme="http://w3id.org/meta-share/meta-share/cordis">219378</ms:ProjectIdentifier>
				<ms:projectName xml:lang="en">European Language Grid</ms:projectName>
				<ms:projectShortName xml:lang="en">ELG</ms:projectShortName>
				<ms:fundingType>http://w3id.org/meta-share/meta-share/euFunds</ms:fundingType>
				<ms:funder>
					<ms:Organization>
						<ms:actorType>Organization</ms:actorType>
						<ms:organizationName xml:lang="en">European Commission</ms:organizationName>
						<ms:website>https://ec.europa.eu/info/index_en</ms:website>
					</ms:Organization>
				</ms:funder>
				<ms:fundingCountry>EU</ms:fundingCountry>
				<ms:projectStartDate>2019-01-01</ms:projectStartDate>
				<ms:projectEndDate>2021-12-31</ms:projectEndDate>
				<ms:website>https://www.european-language-grid.eu/</ms:website>
				<ms:logo>https://www.european-language-grid.eu/wp-content/themes/elg_theme/fab/image/logo/rgb_elg__logo--colour.svg</ms:logo>
				<ms:LTArea>
					<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
				</ms:LTArea>
				<ms:keyword xml:lang="en">Language technology services</ms:keyword>
				<ms:keyword xml:lang="en">Multilingualism</ms:keyword>
				<ms:keyword xml:lang="en">Less-resourced languages</ms:keyword>
				<ms:grantNumber>825627</ms:grantNumber>
				<ms:projectSummary xml:lang="en">With 24 official EU and many more additional languages, multilingualism in Europe and an inclusive Digital Single Market can only be enabled through Language Technologies (LTs). European LT business is dominated by thousands of SMEs and a few large players. Many are world-class, with technologies that outperform the global players. However, European LT business is also fragmented β€“ by nation states, languages, verticals and sectors. Likewise, while much of European LT research is world-class, with results transferred into industry and commercial products, its full impact is held back by fragmentation. The key issue and challenge is the fragmentation of the European LT landscape. The European Language Grid (ELG) project will address this fragmentation by establishing the ELG as the primary platform for LT in Europe. The ELG will be a scalable cloud platform, providing, in an easy-to-integrate way, access to hundreds of commercial and non-commercial Language Technologies for all European languages, including running tools and services as well as data sets and resources. It will enable the commercial and non-commercial European LT community to deposit and upload their technologies and data sets into the ELG, to deploy them through the grid, and to connect with other resources. The ELG will boost the Multilingual Digital Single Market towards a thriving European LT community, creating new jobs and opportunities. Through open calls, up to 20 pilot projects will be financially supported to demonstrate the usefulness of the ELG. The proposal is rooted in the experience of a consortium with partners involved in all relevant initiatives. Based on these, 30\\ national competence centres and the European LT Board will be set up for European coordination. The ELG will foster β€language technologies for Europe built in Europeβ€, tailored to our languages and cultures and to our societal and economical demands, benefitting the European citizen, society, innovation and industry.</ms:projectSummary>
				<ms:cost>
					<ms:amount>7460206.25</ms:amount>
					<ms:currency>http://w3id.org/meta-share/meta-share/euro</ms:currency>
				</ms:cost>
				<ms:ecMaxContribution>
					<ms:amount>6999631.25</ms:amount>
					<ms:currency>http://w3id.org/meta-share/meta-share/euro</ms:currency>
				</ms:ecMaxContribution>
				<ms:fundingSchemeCategory>IA</ms:fundingSchemeCategory>
				<ms:status>SIGNED</ms:status>
				<ms:relatedCall>H2020-ICT-2018-2</ms:relatedCall>
				<ms:relatedProgramme>H2020</ms:relatedProgramme>
				<ms:relatedSubprogramme>ICT-29-2018</ms:relatedSubprogramme>
				<ms:coordinator>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">DEUTSCHES FORSCHUNGSZENTRUM FUR KUNSTLICHE INTELLIGENZ GMBH</ms:organizationName>
					<ms:website>https://www.dfki.de/</ms:website>
				</ms:coordinator>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">SAIL LABS TECHNOLOGY GMBH</ms:organizationName>
					<ms:website>https://www.sail-labs.com/</ms:website>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">THE UNIVERSITY OF SHEFFIELD</ms:organizationName>
					<ms:website>https://www.dfki.de/</ms:website>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">ATHINA-EREVNITIKO KENTRO KAINOTOMIAS STIS TECHNOLOGIES TIS PLIROFORIAS, TON EPIKOINONION KAI TIS GNOSIS</ms:organizationName>
					<ms:website>https://www.athena-innovation.gr/</ms:website>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">EVALUATIONS AND LANGUAGE RESOURCES DISTRIBUTION AGENCY</ms:organizationName>
					<ms:website>http://www.elda.org/</ms:website>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">TILDE SIA</ms:organizationName>
					<ms:website>https://www.tilde.eu/</ms:website>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">UNIVERZITA KARLOVA</ms:organizationName>
					<ms:website>https://www.cuni.cz/</ms:website>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">THE UNIVERSITY OF EDINBURGH</ms:organizationName>
					<ms:website>https://www.ed.ac.uk/</ms:website>
				</ms:participatingOrganization>
				<ms:participatingOrganization>
					<ms:actorType>Organization</ms:actorType>
					<ms:organizationName xml:lang="en">EXPERT SYSTEM IBERIA SL</ms:organizationName>
					<ms:website>http://www.expertsystem.com/</ms:website>
				</ms:participatingOrganization>
			</ms:Project>
		</ms:DescribedEntity>
	</ms:MetadataRecord>

Step 2: upload
--------------

From the ELG catalogue, click the “Upload” link as shown below:

.. figure:: Upload01.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload menu

Now upload the file you created in Step 1:

.. figure:: Upload02.png
   :width: 800px
   :align: center
   :target: https://live.european-language-grid.eu/catalogue/#/
   :alt: Upload metadata XML

If there are any errors in your XML file, these will be shown to you. Fix them and try the upload again. Eventually, a success message will be shown to you and the metadata will be imported into the database.

Step 3: wait for approval
-------------------------

At this stage, the metadata record is only visible to you and to us, the ELG platform administrators. We will check your contribution and integrate it into the ELG catalogue if everything is in order, and contact you otherwise.
