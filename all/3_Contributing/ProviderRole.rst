.. _providerRole:

Register as a provider
######################

In order to contribute to ELG, :ref:`register` to the platform and ask (by email to contact@european-language-grid.eu) to be granted "Provider" permissions.
