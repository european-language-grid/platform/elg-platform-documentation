.. _accessManagement:

Access Management
#################

This page describes processes for the management of accounts on / granting access to infrastructure platforms of the ELG.

For now, please use the general support email address for access inquiries: **contact@european-language-grid.eu**.

The current policy, if not explicitly stated otherwise, is that you have to be an ELG consortium member in order to be eligible for receiving access to either of the following pages or components.


Grid management
***************

Catalogue Backend
=================

The ELG catalogue backend or backend administration is implemented using Django. In this area, we manage resources, users, groups as they appear on the catalogue.

.. image:: catalogue-backend.png
   :width: 500px
   :align: center
   :target: https://www.djangoproject.com/
   :alt: Main page of the ELG Backend Administration


Keycloak
========

All accounts on the ELG are managed in Keycloak. Accounts on the Catalogue Backend (see above) and on the Drupal CMS (see below) are inherited from here.

.. image:: keycloak.png
   :width: 800px
   :align: center
   :target: https://www.keycloak.org/
   :alt: Realm settings page of the "ELG" Keycloak realm


Public information pages
************************

Drupal
======

All pages associated to https://live.european-language-grid.eu/ apart from the ELG catalogue are managed by the Drupal CMS.


WordPress
=========

Information on the ELG, especially regarding Open Calls and Events, is presented on the WordPress website https://www.european-language-grid.eu/ .

.. image:: wordpress-main.png
   :width: 800px
   :align: center
   :alt: Main page (public) of the ELG WordPress website


ReadTheDocs
===========

This documentation is hosted in the `elg-platform-documentation <https://gitlab.com/european-language-grid/platform/elg-platform-documentation.git>`_ GitLab repository.

.. image:: readthedocs.png
   :width: 800px
   :align: center
   :alt: ReadTheDocs administration page


Coding repositories
*******************

GitLab
======

All software produced in the ELG project is developed using Git and (eventually) made available open-source via ELG's GitLab group at https://gitlab.com/european-language-grid. Each partner of the ELG consortium can organise their code into one or multiple repositories.

.. image:: gitlab-group.png
   :width: 800px
   :align: center
   :alt: GitLab group page



Project management
******************

Jira
====

Jira is the ELG's main project management tool for planning and issue tracking. Each of the work packages has its own board.



Slack
=====

Slack is one of the communication channels in the ELG consortium, mostly focused on infrastructure topics and implementation of backend and frontend.

.. image:: slack.png
   :width: 800px
   :align: center
   :alt: Slack channels



Nextcloud
=========

We use Nextcloud as our document storage shared between the ELG consortium members.
