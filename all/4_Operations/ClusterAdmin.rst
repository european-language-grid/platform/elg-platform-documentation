.. _clusterAdmin:

Cluster administration
######################

This page describes processes concerning the administration of the ELG cluster.


Prerequisites
*************

Install the OpenStack client of SysEleven:
https://docs.syseleven.de/syseleven-stack/en/howtos/openstack-cli

Configure API Access for the OpenStack client:
https://docs.syseleven.de/syseleven-stack/en/tutorials/api-access


Namespaces
**********

.. note:: This section will be provided shortly.
