.. _helpdesk:

Helpdesk
########

Please use our `contact form <https://www.european-language-grid.eu/contact/>`_ to send inquiries of any type to the ELG consortium.

.. note:: We will soon provide ways to contact the different task forces of the ELG consortium depending on your type of inquiry (legal, administration, call-related, data & services-related, service & resource integration, platform).
