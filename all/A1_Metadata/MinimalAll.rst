.. _minimalAll:

Minimal elements for all entities
#################################

This page describes the minimal metadata elements common to all types of entities.

----

MetadataRecord 
===================
:guilabel:`Path`	``MetadataRecord``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

A set of formalized structured information used to describe the contents, structure, function, etc. of an entity, usually according to a specific set of rules (metadata schema)
	
The ``MetadataRecord`` element includes a set of administrative data, of which the main elements (**automatically assigned by the ELG software**) for metadata records registered by individuals are:

- ``metadataCreator``: the person that has created the metadata record
- ``metadataCurator``: the person that will be assigned the responsibility to update the metadata record when imported in the ELG database; it is usually the same peroson as the metadataCreator
- ``metadataCreationDate``: the date when the metadata record was created
- ``compliesWith``: for ELG metadata records, this is by default the ELG-SHARE metadata schema


:guilabel:`Example`

.. code-block:: xml

    <ms:MetadataRecord>
        <ms:MetadataRecordIdentifier ms:MetadataRecordIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">default id</ms:MetadataRecordIdentifier>
        <ms:metadataCreationDate>2020-02-28</ms:metadataCreationDate>
        <ms:metadataCurator>
                <ms:actorType>Person</ms:actorType>
                <ms:surname xml:lang="en">Smith</ms:surname>
                <ms:givenName xml:lang="en">John</ms:givenName>
        </ms:metadataCurator>
        <ms:compliesWith>http://w3id.org/meta-share/meta-share/ELG-SHARE</ms:compliesWith>
        <ms:metadataCreator>
                <ms:actorType>Person</ms:actorType>
                <ms:surname xml:lang="en">Brown</ms:surname>
                <ms:givenName xml:lang="en">George</ms:givenName>
        </ms:metadataCreator>
    </ms:metadataRecord>