.. _minimalLexConc:

Minimal elements for lexical/conceptual resources
#################################################

This page describes the minimal metadata elements specific to **lexical/conceptual resources**.

----

LexicalConceptualResource
============================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.LexicalConceptualResource``	

:guilabel:`Data type`	component	

:guilabel:`Optionality`	Mandatory	

:guilabel:`Explanation & Instructions`

Wraps together elements for lexical/conceptual resources

:guilabel:`Example`

.. code-block:: xml

	<ms:LRSubclass>
		<ms:LexicalConceptualResource>
			<ms:lrType>LexicalConceptualResource</ms:lrType>
			...
		</ms:LexicalConceptualResource>
	</ms:LRSubclass>
	
----

lcrSubclass
============================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.LexicalConceptualResource.lcrSubclass``	

:guilabel:`Data type`	CV (`lcrSubclass <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#lcrSubclass>`_)

:guilabel:`Optionality`	Recommended	

:guilabel:`Explanation & Instructions`

Introduces a classification of lexical/conceptual resources into types (used for descriptive reasons)

:guilabel:`Example`

.. code-block:: xml

	<lcrSubclass>http://w3id.org/meta-share/meta-share/computationalLexicon</lcrSubclass>

	<lcrSubclass>http://w3id.org/meta-share/meta-share/ontology</lcrSubclass>

----

encodingLevel
============================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.LexicalConceptualResource.encodingLevel``	

:guilabel:`Data type`	CV	(`encodingLevel <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#encodingLevel>`_)

:guilabel:`Optionality`	Mandatory	

:guilabel:`Explanation & Instructions`

Classifies the contents of a lexical/conceptual resource or language description as regards the linguistic level of analysis it caters for

You can repeat the element for multiple encoding levels.

:guilabel:`Example`

.. code-block:: xml


	<ms:encodingLevel>http://w3id.org/meta-share/meta-share/phonology</ms:encodingLevel>

	<ms:encodingLevel>http://w3id.org/meta-share/meta-share/semantics</ms:encodingLevel>
	
-----

ContentType
============================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.LexicalConceptualResource.ContentType``	

:guilabel:`Data type`	CV	(`ContentType <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#ContentType>`_)

:guilabel:`Optionality`	Mandatory	

:guilabel:`Explanation & Instructions`

A more detailed account of the linguistic information contained in the lexical/conceptual resource

You can repeat the element for multiple encoding levels.

:guilabel:`Example`

.. code-block:: xml

	<ms:ContentType>http://w3id.org/meta-share/meta-share/collocation</ms:ContentType>

	<ms:ContentType>http://w3id.org/meta-share/meta-share/definition</ms:ContentType>

-----

compliesWith
============================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.LexicalConceptualResource.ContentType``	

:guilabel:`Data type`	CV	(`compliesWith <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#compliesWith>`_)

:guilabel:`Optionality`	Mandatory	

:guilabel:`Explanation & Instructions`

Specifies the vocabulary/standard/best practice to which a resource is compliant with

You can repeat the element for multiple encoding levels.

:guilabel:`Example`

.. code-block:: xml

	<ms:compliesWith>http://w3id.org/meta-share/meta-share/LMF</ms:compliesWith>

-----

LexicalConceptualResourceTextPart	
======================================
:guilabel:`Path`	``MetadataRecord.DescribedEntity.LanguageResource.LRSubclass.Corpus.CorpusMediaPart.LexicalConceptualResourceTextPart``	

:guilabel:`Data type`		component

:guilabel:`Optionality`	Mandatory if applicable	

:guilabel:`Explanation & Instructions`

A part (or whole set) of a lexical/conceptual resource that consists of textual elements

You can repeat the group of elements for multiple textual parts.

The mandatory or recommended elements for the text part of lexical/conceptual resources are:
			
	- ``mediaType`` (Mandatory): Specifies the media type of a language resource (the physical medium of the contents representation). For text parts, always use the value 'text'.

	- ``lingualityType`` (Mandatory	): Indicates whether the resource includes one, two or more languages.

	- ``multilingualityType`` (Mandatory if applicable): Indicates whether the resource (part) is parallel, comparable or mixed. If lingualityType = bilingual or multilingual, it is required; select one of the values for parallel (e.g., original text and its translations), comparable (e.g. corpus of the same domain in multiple languages) and multilingualSingleText (for corpora that consist of segments including text in two or more languages (e.g., the transcription of a European Parliament session with MPs speaking in their native language).

	- ``language`` (Mandatory): Specifies the language that is used in the resource part , expressed according to the BCP47 recommendation. See :ref:`language`.

	- ``languageVariety`` (Mandatory if applicable): Relates a language resource that contains segments in a language variety (e.g., dialect, jargon) to it. Please use for dialect corpora.
	
	- ``metalanguage`` (Mandatory): Specifies the language that is used in the resource part , expressed according to the BCP47 recommendation. See :ref:`language`.

	- ``modalityType`` (Recommended if applicable): Specifies the type of the modality represented in the resource. For instance, you can use 'spoken language' to describe transcribed speech corpora.
	

:guilabel:`Example`

.. code-block:: xml

	<ms:LexicalConceptualResourceMediaPart>
		<ms:LexicalConceptualResourceTextPart>
			<ms:lcrMediaType>LexicalConceptualResourceTextPart</ms:lcrMediaType>
			<ms:mediaType>http://w3id.org/meta-share/meta-share/text</ms:mediaType>
			<ms:lingualityType>http://w3id.org/meta-share/meta-share/bilingual</ms:lingualityType>
			<ms:multilingualityType>http://w3id.org/meta-share/meta-share/parallel</ms:multilingualityType>
			<ms:language>
				<ms:languageTag>en-US</ms:languageTag>
				<ms:languageId>en</ms:languageId>
				<ms:regionId>US</ms:regionId>
			</ms:language>
			<ms:language>
				<ms:languageTag>es</ms:languageTag>
				<ms:languageId>es</ms:languageId>
			</ms:language>
			<ms:metalanguage>
				<ms:languageTag>es</ms:languageTag>
				<ms:languageId>es</ms:languageId>
			</metalanguage>
			</ms:language>
		</ms:LexicalConceptualResourceTextPart>
	</ms:LexicalConceptualResourceMediaPart>