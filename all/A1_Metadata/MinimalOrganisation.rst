.. _minimalOrganisation:

Minimal elements for organisations
##################################

This page describes the minimal metadata elements specific to **organisations**.

----

Organization
============

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

Wraps together elements for organizations

:guilabel:`Example`

.. code-block:: xml

	<ms:Organization>
		<ms:entityType>organization</ms:entityType>
		...
	</ms:Organization>

----

OrganizationIdentifier 
==========================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.OrganizationIdentifier``

:guilabel:`Data type`	string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

A string (e.g., PID, internal to an organization, issued by the funding authority, etc.) used to uniquely identify an organization

You must also use the attribute ``OrganizationIdentifierScheme`` to specify the name of the scheme according to which an identifier is assigned to an organization by the authority that issues it. See `OrganizationIdentifierScheme <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#OrganizationIdentifierScheme>`_ for details.

:guilabel:`Example`

.. code-block:: xml

	<ms:OrganizationIdentifier ms:OrganizationIdentifierScheme="http://w3id.org/meta-share/meta-share/elg">automatically assigned by ELG - please don't change</ms:OrganizationIdentifier>

----

organizationName 
====================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.organizationName``

:guilabel:`Data type`	multilingual string

:guilabel:`Optionality`	Mandatory

:guilabel:`Explanation & Instructions`

The full name of an organization

:guilabel:`Example`

.. code-block:: xml

	<ms:organizationName xml:lang="en">Charles University</ms:organizationName>

	<ms:organizationName xml:lang="en">Evaluation and Language Resources Distribution Agency</ms:organizationName>

----

organizationShortName 
=========================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.organizationShortName``

:guilabel:`Data type`	multilingual string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Introduces the short name (abbreviation, acronym , etc.) used for an organization

:guilabel:`Example`

.. code-block:: xml

	<ms:organizationShortName xml:lang="en">CUNI</ms:organizationName>

	<ms:organizationShortName xml:lang="en">ELDA</ms:organizationName>


----

organizationAlternativeName 
===============================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.organizationAlternativeName``

:guilabel:`Data type`	multilingual string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Introduces an alternative name (other than the short name) used for an organization

:guilabel:`Example`

.. code-block:: xml

	<ms:organizationAlternativeName xml:lang="en">UNIVERZITA KARLOVA</ms:organizationAlternativeName>

	<ms:organizationAlternativeName xml:lang="en">EVALUATIONS AND LANGUAGE RESOURCES DISTRIBUTION AGENCY</ms:organizationAlternativeName>

----

organizationBio 
===================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.organizationBio``

:guilabel:`Data type`	multilingual string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Introduces a short free-text account that provides information on an organization

:guilabel:`Example`

.. code-block:: xml

	<ms:organizationBio xml:lang="en">Charles University was founded in 1348, making it one of the oldest universities in the world. Yet it is also renowned as a modern, dynamic, cosmopolitan and prestigious institution of higher education. It is the largest and most renowned Czech university, and is also the best-rated Czech university according to international rankings. There are currently 17 faculties at the University (14 in Prague, 2 in Hradec KrΓ΅lovΓ© and 1 in PlzeΕ), plus 3 institutes, 6 other centres of teaching, research, development and other creative activities, a centre providing information services, 5 facilities serving the whole University, and the Rectorate - which is the executive management body for the whole University.</ms:organizationBio>

	<ms:organizationBio xml:lang="en">The Evaluations and Language Resources Distribution Agency (ELDA), was created in 1995 as the organizational infrastructure with the mission of  providing a central clearing house for Language Resources (LR) of the  European Language Resources Association (ELRA). ELDA was set up to  identify, classify, collect, validate and distribute the language resources that are  needed by the Human Language Technology (HLT) community. Anticipating the   evolutions in the HLT field, ELDA broadened its activities to cover  multimedia/multimodal resources as well as evaluation activities, distributing  the language resources needed for evaluation purposes, and conducting/coordinating evaluation campaigns. ELDA has played a significant role within the major Multimedia and Multimodal production projects that resulted in one of the most impressive catalogues of available data sets, embracing all aspects of Language Technologies. ELDA was also involved in evaluation initiatives, in several FPs’ projects involving HLT infrastructures, as well as in national programmes. In addition to work on data production, processing and annotation, validation and quality control, several of these projects also involved work on legal framework management for the produced resources. Moreover, ELDA has contributed to the development of open platforms and has joined forces with other European key players by bringing its assets (LR catalogue, evaluation services and benchmarking) to constitute Europe's backbone for Language Resources sharing and distribution. ELDA is also the initiator of the Language Resource and the Evaluation Conference (LREC), since 1998. With over 1200 participants, LREC is the major event on Language Resources (LRs) and Evaluation for Human Language Technologies (HLT).</ms:organizationBio>

----

logo
=====

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.logo``

:guilabel:`Data type`	URL

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Links to a URL with an image file containing a symbol or graphic object used to identify the entity

:guilabel:`Example`

.. code-block:: xml

	<ms:logo>https://cuni.cz/UKEN-1-version1-afoto.jpg</ms:logo>

	<ms:logo>https://www.european-language-grid.eu/wp-content/uploads/2019/03/logo__consortium-elda.svg</ms:logo>

----

LTArea 
==========

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.LTArea``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Introduces a Language Technology-related area that a person or organization is involved or active in

For details, see `LTArea <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#LTArea>`_
More specifically, you can fill in:

- the ``LTClassRecommended`` element with one of the recommended values from the `LT taxonomy <https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#LTClassRecommended>`_, or
- the ``LTClassOther`` element with a free text.


:guilabel:`Example`

.. code-block:: xml

	<ms:LTArea>
		<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LanguageTechnology</ms:LTClassRecommended>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
	</ms:LTArea>

----

serviceOffered 
==================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.serviceOffered``

:guilabel:`Data type`	multilingual string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Lists the service(s) offered by an organization or person

:guilabel:`Example`

.. code-block:: xml

	<ms:serviceOffered xml:lang="en">Evaluation and benchmarking</ms:serviceOffered>
	<ms:serviceOffered xml:lang="en">Legal support</ms:serviceOffered>

domain 
==========

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.domain``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Identifies a domain that the organization deals with

You must fill in the ``CategoryLabel`` element with a free text value. If you prefer to add a value from an established controlled vocabulary, you can also use the ``DomainIdentifier`` (with the attribute ``DomainClassificationScheme`` with the appropriate value).

:guilabel:`Example`

.. code-block:: xml

			<ms:domain>
				<ms:categoryLabel xml:lang="en">environment</ms:categoryLabel>
			</ms:domain>


keyword 
===========

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.keyword``

:guilabel:`Data type`	multilingual string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Introduces a word or phrase considered important for the description of the project and thus used to index or classify it

:guilabel:`Example`

.. code-block:: xml

	<ms:keyword xml:lang="en">Computational Linguistics</ms:keyword>
	<ms:keyword xml:lang="en">Natural Language Processing</ms:keyword>
	<ms:keyword xml:lang="en">Language Resources</ms:keyword>
	<ms:keyword xml:lang="en">Research infrastructures</ms:keyword>
	<ms:keyword xml:lang="en">Language Resources</ms:keyword>
	<ms:keyword xml:lang="en">Digital Humanities</ms:keyword>

	<ms:keyword xml:lang="en">Language Resources and Evaluation</ms:keyword>
	<ms:keyword xml:lang="en">Legal support</ms:keyword>
	<ms:keyword xml:lang="en">Data management</ms:keyword>

----

email 
=========

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.email``

:guilabel:`Data type`	string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Points to the email address of a person, organization or group

:guilabel:`Example`

.. code-block:: xml

		<ms:email>info@elda.org</ms:email>

----

website 
===========

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.website``

:guilabel:`Data type`	URL

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Links to a URL that acts as the primary page (like a table of contents) introducing information about an organization (e.g., products, contact information, etc.) or project

:guilabel:`Example`

.. code-block:: xml

	<ms:website>https://www.cuni.cz</ms:website>

	<ms:website>http://www.elra.info/en/</ms:website>

----

headOfficeAddress 
===================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.headOfficeAddress``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Links to a set of elements that describe the full address of the head office of an or organization (i.e. including street address, zip code, etc.). The only mandatory element in this set is ``country``.

:guilabel:`Example`

.. code-block:: xml

	<ms:headOfficeAddress>	
		<ms:address xml:lang="en">OLD COLLEGE, SOUTH BRIDGE</ms:address>
		<ms:zipCode>EH8 9YL</ms:zipCode>
		<ms:city xml:lang="en">EDINBURGH</ms:city>
		<ms:country>GB</ms:country>
	</ms:headOfficeAddress>	

----

socialMediaOccupationalAccount 
==================================

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.socialMediaOccupationalAccount``

:guilabel:`Data type`	multilingual string

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Introduces the social media or occupational account details of a person or organization

You must also use the attribute ``socialMediaAccountType`` to specify the type of social media account. See https://european-language-grid.readthedocs.io/en/release1.1.0/Documentation/ELG-SHARE_xsd.html#socialMediaOccupationalAccountType for details.

:guilabel:`Example`

.. code-block:: xml

	<ms:socialMediaOccupationalAccount ms:socialMediaOccupationalAccountType="http://w3id.org/meta-share/meta-share/facebook">https://www.facebook.com/UFALMFFUK</ms:socialMediaOccupationalAccount>

hasDivision 
===============

:guilabel:`Path`	``MetadataRecord.DescribedEntity.Organization.hasDivision``

:guilabel:`Data type`	component

:guilabel:`Optionality`	Recommended

:guilabel:`Explanation & Instructions`

Links an organization to the division(s) it consists of

:guilabel:`Example`

.. code-block:: xml

	<ms:hasDivision>
		<ms:divisionName xml:lang="en">Institute of Formal and Applied Linguistics</ms:divisionName>
		<ms:divisionShortName xml:lang="en">UFAL</ms:divisionShortName>
		<ms:divisionCategory>http://w3id.org/meta-share/meta-share/institute</ms:divisionCategory>
		<ms:organizationBio xml:lang="en">'Institute of Formal and Applied Linguistics (ΓFAL) at the Computer Science School, Faculty of Mathematics and Physics, Charles University, Czech Republic.  The institute was established in 1990 as a continuation of the research  and teaching activities carried out by the former Laboratory of Algebraic Linguistics since the early 60s at the Faculty of Philosophy and later at the Faculty of Mathematics and Physics, Charles University. The Institute is a primarily research department working on many topics in the area of Computational Linguistics, and on many research projects both nationally and internationally. However, the Institute of Formal and Applied Linguistics is also a regular department in the sense that it carries a comprehensive teaching program both for the Master''s degree (Mgr., or MSc.) as well as for a doctorate (Ph.D.) in Computational Linguistics. Both programs are taught in Czech and English. The Institute is also a member of the double- degree \"Master''s LCT programme\" of the EU. Students also can take advantage of the Erasmus program for typically semester-long stays at partner Universities abroad. '</ms:organizationBio>
		<ms:logo>https://ufal.mff.cuni.cz/sites/all/themes/drufal/css/logo/logo_ufal_110u.png</ms:logo>
	<ms:LTArea>
		<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/MachineTranslation</ms:LTClassRecommended>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/SpeechRecognition</ms:LTClassRecommended>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/Annotation</ms:LTClassRecommended>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassRecommended>http://w3id.org/meta-share/omtd-share/LexiconCreation</ms:LTClassRecommended>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassOther>Lexical Resources</ms:LTClassOther>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassOther>Dialog systems</ms:LTClassOther>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassOther>Corpus Creation</ms:LTClassOther>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassOther>Research Infrastructure</ms:LTClassOther>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassOther>LT services</ms:LTClassOther>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassOther>NLP Support</ms:LTClassOther>
	</ms:LTArea>
	<ms:LTArea>
		<ms:LTClassOther>Digital Humanities</ms:LTClassOther>
	</ms:LTArea>
	<ms:keyword xml:lang="en">Computational Linguistics</ms:keyword>
		<ms:addressSet>
			<ms:address xml:lang="en">MalostranskΓ© nΓ΅m. 25</ms:address>
			<ms:zipCode>11800</ms:zipCode>
			<ms:city xml:lang="en">Praha 1</ms:city>
			<ms:country>CZ</ms:country>
		</ms:addressSet>
	</ms:hasDivision>