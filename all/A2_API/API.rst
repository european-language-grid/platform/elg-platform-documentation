.. API:

API specifications
==================

This annex contains the specifications of ELG's internal and public application programming interfaces (APIs).