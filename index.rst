.. European Language Grid documentation master file, created by
   sphinx-quickstart on Thu Apr  9 13:48:29 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==============================================================
European Language Grid – User Manual
==============================================================

Welcome to the user manual of the European Language Grid.

.. toctree::
   :maxdepth: 2
   :caption: Chapter 1: Introduction

   ./all/1_Introduction/Introduction.rst
   ./all/1_Introduction/TypesOfResources.rst

.. toctree::
   :maxdepth: 2 
   :caption: Chapter 2: Using ELG

   ./all/2_Using/Using.rst
   ./all/2_Using/Browse.rst
   ./all/2_Using/RegisterAsUser.rst
   ./all/2_Using/TestService.rst
   ./all/2_Using/Down.rst
   
.. toctree::
   :maxdepth: 2 
   :caption: Chapter 3: Contributing to ELG

   ./all/3_Contributing/Contributing.rst
   ./all/3_Contributing/ProviderRole.rst
   ./all/3_Contributing/Service.rst
   ./all/3_Contributing/Downloadable.rst
   ./all/3_Contributing/Corpus.rst
   ./all/3_Contributing/Model.rst
   ./all/3_Contributing/Grammar.rst
   ./all/3_Contributing/LexConc.rst
   ./all/3_Contributing/Project.rst
   ./all/3_Contributing/Organisation.rst
   ./all/3_Contributing/UpdateResource.rst
   ./all/3_Contributing/External.rst

.. toctree::
   :maxdepth: 2 
   :caption: Annex 1: Metadata schema

   ./all/A1_Metadata/Metadata.rst
   ./all/A1_Metadata/MinimalAll.rst
   ./all/A1_Metadata/MinimalLRT.rst
   ./all/A1_Metadata/MinimalToolService.rst
   ./all/A1_Metadata/MinimalCorpora.rst
   ./all/A1_Metadata/MinimalLangDesc.rst
   ./all/A1_Metadata/MinimalLexConc.rst
   ./all/A1_Metadata/MinimalProject.rst
   ./all/A1_Metadata/MinimalOrganisation.rst

.. toctree::
   :maxdepth: 2
   :caption: Annex 2: API specifications

   ./all/A2_API/API.rst
   ./all/A2_API/LTInternalAPI.rst
   ./all/A2_API/LTPublicAPI.rst

.. toctree::
   :maxdepth: 2
   :caption: Annex 3: Processes and policies

   ./all/A3_ProcessesPolicies/TermsOfUse.rst

.. toctree::
   :maxdepth: 2
   :caption: Annex 4: Publications and reports

   ./all/A4_Publications/Publications.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
